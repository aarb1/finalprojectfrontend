import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TraderTable } from '../view/trader-table';
import { TraderToolbar } from '../view/trader-toolbar';
import { TraderPipe } from 'src/model/traderpipe';
// import { GraphComponent } from './graph/graph.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { GraphProfitComponent } from '../view/graph-profit/graph-profit.component';
// import { CanvasJS } from 'node_modules/canvasjs';
// import { Jquery } from 'node_modules/jquery';

@NgModule({
  declarations: [
    AppComponent,
    TraderTable,
    TraderToolbar,
    TraderPipe,
    GraphProfitComponent,
    //GraphComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    //CanvasJS,
   // Jquery
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
