import { Component } from "@angular/core";
import * as $ from 'jquery';
import * as CanvasJS from '../assets/canvasjs.min';
//import { CanvasJS } from 'node_modules/canvasjs';
import { TraderTable } from "../view/trader-table";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "Automated Trading Platform";

}
