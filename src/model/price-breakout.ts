import { Position } from "./position";
import { Trader } from "./trader";

/**
 * Serializable/deserializable encapsulation of a PB strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Winston Liu
 */
export class PriceBreakout extends Trader {
  timePeriod: number;
  repeat: number;
  exitThreshold: number;

  constructor(ID: number, stock: string, size: number, active: boolean,
      stopping: boolean, positions: Array<Position>, profitOrLoss: number, ROI: number,
      timePeriod: number, repeat: number,  exitThreshold: number) {
    super("PB", ID, stock, size, active, stopping, positions, profitOrLoss, ROI);
    this.timePeriod = timePeriod;
    this.repeat = repeat;
    this.exitThreshold = exitThreshold;
  }
}
