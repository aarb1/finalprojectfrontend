import { Pipe, PipeTransform } from '@angular/core';
import { Trader } from './trader';

@Pipe({
    name: 'traderpipe',
    pure: false
})
export class TraderPipe implements PipeTransform {
    transform(items: Trader[], traderType: string): Trader[] {
        if (!items || !traderType) {
            return items;
        }
        // filter trader array, trader with designated type will be kept, false will be filtered out
        return items.filter(item => item["@type"] == traderType);
    }
}