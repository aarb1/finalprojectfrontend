import { Position } from "./position";
import { Trader } from "./trader";

/**
 * Serializable/deserializable encapsulation of a BB strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Winston Liu
 */
export class BollingerBand extends Trader {
  timePeriod: number;
  multiple: number;
  exitThreshold: number;

  constructor(ID: number, stock: string, size: number, active: boolean,
      stopping: boolean, positions: Array<Position>, profitOrLoss: number, ROI: number,
      timePeriod: number, multiple: number,  exitThreshold: number) {
    super("BB", ID, stock, size, active, stopping, positions, profitOrLoss, ROI);
    this.timePeriod = timePeriod;
    this.multiple = multiple;
    this.exitThreshold = exitThreshold;
  }
}
