import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';
import * as CanvasJS from '../../assets/canvasjs.min';
//import { CanvasJS } from 'node_modules/canvasjs';
import { TraderTable } from "../../view/trader-table";
import { callbackify } from 'util';

@Component({
  selector: 'app-graph-profit',
  templateUrl: './graph-profit.component.html',
  styleUrls: ['./graph-profit.component.css']
})
export class GraphProfitComponent implements OnInit {

  @Input() totalProfit

  dataPoints;
  countY;
  dpsLength;
  chart;


  constructor() { }

  ngOnInit() {
    this.dataPoints=[];
    this.dpsLength = 0;
    this.countY = 0;
    this.chart = new CanvasJS.Chart("chartContainer",{
      theme: "dark1",
      exportEnabled: true,
      title:{
        text:"Total Profit"
      },
      data: [{
        type: "spline",
        dataPoints : this.dataPoints,
      }]
    });
    
      for(let element in this.totalProfit){
        this.dataPoints.push({x:this.countY, y:this.totalProfit[element]})
      }
      this.dpsLength = this.dataPoints.length;
      this.chart.render();
      this.updateChart();


    }



    updateChart(){	
     
        this.dataPoints.push({x:this.countY, y: this.totalProfit[this.totalProfit.length-1]});
        this.countY++
      // for(let element in this.totalProfit){
      //   this.dataPoints.push({x:this.countY, y: this.totalProfit[element]});
      //   this.countY++
        console.log('printing totalProfit last bit ' + this.totalProfit[this.totalProfit.length-1]);
        console.log('printing totalProfit ' + this.totalProfit);
        console.log('printing county' + this.countY);
      // }

       
        
        if (this.dataPoints.length >  20 ) {
              this.dataPoints.shift();				
            }
        this.chart.render();

      

         setTimeout(()=>{this.updateChart()
          
         }, 15000);
          }

          // window.setInterval(function(){
          //   this.updateChart();
          // },1500);

      }



