import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphProfitComponent } from './graph-profit.component';

describe('GraphProfitComponent', () => {
  let component: GraphProfitComponent;
  let fixture: ComponentFixture<GraphProfitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphProfitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphProfitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
