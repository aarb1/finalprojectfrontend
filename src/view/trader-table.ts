import { Component } from "@angular/core";
import { Trader } from "../model/trader";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { TraderService } from "../model/trader-service";
import { TraderUpdate } from "../model/trader-service";
import { Trade } from 'src/model/trade';
import { RadioControlValueAccessor } from '@angular/forms';

//enumerator of Trader status
enum TraderState {
  STARTED = "Started",
  STOPPED = "Stopped",
  STOPPING = "Stopping"
};

/**
 * Angular component showing all current traders, including their
 * type, parameters, state, and profitability.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-table",
  templateUrl: "./trader-table.html",
  styleUrls: ["./trader-table.css"]
})
export class TraderTable implements TraderUpdate {

  service: TraderService;
  traders: Array<Trader> = [];

  display: boolean = false;
  
  traderID: number = 0;


  /**
   * Helper to format times in mm:ss format.
   */
  minSec(millis: number): string {
    let seconds = millis / 1000;
    const minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    const pad = seconds < 10 ? "0" : "";
    return "" + minutes + ":" + pad + seconds;
  }
 // total profit array collecting from getTotalProfit method 
  totalProfitArray = []

  /**
   * Store the injected service references.
   */
  constructor(service: TraderService) {
    this.service = service;
    service.subscribe(this);
    service.notify();
  }

  /**
   * Replace our array of traders with the latest,
   * which will triger a UI update.
   * @author: Winston Liu
   */
  latestTraders(traders: Array<Trader>) {
    this.traders = traders;
  }

  /**
   * Helper to derive a label for the trader's state: "Started",
   * "Stopped", or, if deactivated but still closing out a position,
   * "Stopping".
   */
  getState(trader: Trader) {
    if (trader.stopping) {
      return TraderState.STOPPING;
    } else {
      if (trader.active) {
        return TraderState.STARTED;
      } else {
        return TraderState.STOPPED;
      }
    }
  }

  /**
   * Helper to derive the total number of trades made by this trader.
   */
  getTotalTrades(): number {
    return this.traders.map(t => t.trades).reduce((x, y) => x + y, 0);
  }

  /**
   * Helper to derive the trader's total profit.
   */
  getTotalProfit(): number {
    let profit = this.traders.map(t => t.profitOrLoss).reduce((x, y) => x + y, 0);
    if(profit != 0){
    this.totalProfitArray.push(profit);
    }
    return profit;
  }


  /**
   * Finds the trader at the given table index and uses the
   * TraderService component to send an HTTP request to toggle the
   * trader's state.
   * @author Winston Liu
   */
  startOrStop(trader: Trader, hardStop: boolean) {
    //const index = ev.target.id.replace("trader", "");
    //const trader = this.traders[index];

    console.log(trader.ID + " 's current active status is " + trader.active + ",  will  pass reverse status to backend");

    this.service.setActive(trader.ID, !trader.active);
  }

  //display trader history
  //@author: Winston Liu 
  display_history(id: number) {
    this.traderID = id;
    console.log(this.traderID, id)
    console.log(this.getTrader(id).positions);
    // let whichTrader = this.traders[id]
    // console.log(whichTrader)
    this.display = true;
    
    //console.log("nico dream").
  }

  //get trader by traderID (helper function)
  //@author: Winston Liu
  getTrader(id: Number) {
    for (var trader of this.traders) {
      if (trader.ID == id) {
        return trader;
      }
    }
    console.log("Nico loves Maki!")
    return null;
  }

}
