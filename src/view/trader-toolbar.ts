import { Component } from "@angular/core";
import { Trader } from "../model/trader";
import { TwoMovingAverages } from "../model/two-moving-averages";
import { TraderService } from "../model/trader-service";
import { BollingerBand } from 'src/model/bollinger-band';
import { PriceBreakout } from 'src/model/price-breakout';

/**
 * Angular component for a toolbar that allows the user to configure
 * and to create new traders. The component logic and the HTML template
 * currently support only the 2MA trader type.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-toolbar",
  templateUrl: "./trader-toolbar.html",
  styleUrls: ["./trader-toolbar.css"]
})
export class TraderToolbar {

  service: TraderService;
  type: string;
  stock: string;
  size: number;

  //2MA
  lengthShort: number;
  lengthLong: number;

  //BB
  timePeriod: number;
  multiple: number;

  //PB
  repeat: number;

  exitThreshold: number;

  /**
   * Set default values for all properties, which will flow out to the
   * initial UI via two-way binding.
   */
  constructor(service: TraderService) {
    this.service = service;

    this.type = "2MA";
    this.stock = "MRK";
    this.size = 1000;

    //2MA
    this.lengthShort = 30;
    this.lengthLong = 60;

    //BB
    this.timePeriod = 60;
    this.multiple = 2;

    //PB
    this.repeat = 2;

    this.exitThreshold = 3;
  }

  /**
   * Reads the values of form controls via two-way binding.
   * Creates an instance of the trader (only 2MA traders currently supported)
   * and sends it to the server to be created and activated.
   */
  create() {
    console.log(this.type);

    if (this.type == "2MA") {

      console.log("2MA has short period of " + this.lengthShort + " and long period of " + this.lengthLong);

    this.service.createTrader(new TwoMovingAverages
      (0, this.stock, this.size, true, false, [], 0, NaN,
        this.lengthShort * 1000, this.lengthLong * 1000, this.exitThreshold / 100));
    } else if (this.type == "BB") {

      console.log("BB has time period of " + this.timePeriod + " and multiple of " + this.multiple);

      this.service.createTrader(new BollingerBand
        (0, this.stock, this.size, true, false, [], 0, NaN,
          this.timePeriod * 1000, this.multiple, this.exitThreshold / 100));
    }  else if (this.type == "PB") {

      console.log("PB has time period of " + this.timePeriod + " and repeat of " + this.repeat);

      if (this.timePeriod/15 < this.repeat +2) {

        window.alert("Repetition time is too large! try smaller one!");

      } else {

        this.service.createTrader(new PriceBreakout
          (0, this.stock, this.size, true, false, [], 0, NaN,
          this.timePeriod * 1000, this.repeat, this.exitThreshold / 100));
        }
    } 
  }
}
